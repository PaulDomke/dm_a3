import math
import random
import statistics
import math
import copy
import csv


##########################################################
####DATA PROCESSING AND DATASTRUCTURE FORMATION######
##########################################################

data = None

with open(sys.argv[1], 'r') as f:
    reader = csv.reader(f)
    data = list(reader)

#creating a dictionary with all possible values each attribute can take on
#keys are columns, not strings for ease of algorithm writing
data_dict = {}
for i, string in enumerate(data[0]):
    data_dict[string] = set([sublist[i] for sublist in data[1:]])

    
#defining a class that we can work with in creating a tree
class Tree(object):
    def __init__(self, children = {}, data = None):
        self.children = children
        if data != None:
            self.data = data

    def classifySingle(self, example, attributes):
        attribute_to_check = self.data
        index = attributes.index(attribute_to_check)
        value = example[index]
        subtree = self.children[value]

        if subtree == self:
            print('LOOP ERROR')
            self.children[value] = 'Male'
            self.classifySingle(example, attributes)
            
        if(isinstance(subtree, Tree) == False):
            return subtree
        else:
            return subtree.classifySingle(example, attributes)

    def toDict(self):
        subdicts = {}
        for entry in self.children:
            if isinstance(self.children[entry], str) == True:
                subdicts[entry] = self.children[entry]
            else:
                subdicts[entry] = self.children[entry].toDict()
        return {self.data : subdicts}

class PrintTree:
    def printTree(self, tree, d = 0):
        if (tree == None or len(tree) == 0):
            print ("|\t" * d, "-")
        else:
            for key, val in tree.items():
                if (isinstance(val, dict)):
                    print("|\t" * d, key)
                    self.printTree(val, d+1)
                else:
                    print("|\t" * d, key, str('(') + val + str(')'))
        
##############################################################
#####INFORMATION, ENTROPY, AND HELPER FUNCTS################
##############################################################


# This calculates the total entropy of a data set by taking the target and all the data
def Entropy(examples, target):
    #This dict will keep track of all the values that the
    #target holds and how many times they appear
    allvalues = {}
    
    #this looks at the values of the target and adds them to the dict
    for example in examples:
        targetval = example[target]
        if not targetval in allvalues:
            allvalues[targetval] = 1
        else:
            allvalues[targetval] = allvalues[targetval] + 1

    #This calcs the total entropy
    totalentropy = 0
    for value in allvalues.values():
        prob = value / len(examples)
        if prob == 0:
            ent = 0
        else:
            ent = -(prob * math.log2(prob))
        totalentropy = totalentropy + ent

    return totalentropy
            
    
#This partions the vector D according to some other vector
#A and then from each of thise we calc the entropy
#and take the weighted avg of that entrpoy
def EntropyForPart(examples, attribute, target):
    
    dictoflist = {}
    counter = 0

    #This splits the data on some attribute and divides them
    #and keeps track of the total examples
    for example in examples:
        counter = counter + 1
        if example[attribute] not in dictoflist:
            dictoflist[example[attribute]] = []
            dictoflist[example[attribute]].append(example)
        else:
            dictoflist[example[attribute]].append(example)

    #this calculates the entropy for a partition
    totalent = 0
    for value in dictoflist.values():
        totalent = totalent + len(value)/counter * Entropy(value, target)
    return totalent


#this calculates the attribute with the best information gain
def ChooseAttribute(attributes, examples, target):

    targetcol = attributes.index(target)
    originalentropy = Entropy(examples, targetcol)
    greatestinfo = -1
    bestattribute = -1
    
    for attribute in attributes:
        if attribute != target:
            #print(attribute, target)
            ent = EntropyForPart(examples, attributes.index(attribute), targetcol)
            test = originalentropy - ent
            if test >= greatestinfo:
                greatestinfo = test
                bestattribute = attributes.index(attribute)
                    
    if bestattribute == -1:
        print(len(examples), len(attributes))
        print("ATTRIBUTE ERROR")
    else:
        return bestattribute




#dont know if we actually need this..

#Returns a set of subsets of datapoints
# such that all points in a subset have
# the same value of the indicated attribute.
def MakeSubsets(examples, attribute):
    #assuming attribute is a number indicating column index starting at 0
    #if not, make a dictionary mapping column names to indices
    value_sets = dict()
    for example in examples:
        if example[attribute] not in value_sets:
            #setting key:value to example's attribute value : list of examples that fit this
            value_sets[example[attribute]] = [example]
        else:
            value_sets[example[attribute]].append(example)
    
    return list(value_sets.values())

def ComputeMode(examples, target):
    if len(examples) > 0:
        stats = {}
        examples = [x[target] for x in examples]
        for example in examples:
            if example not in stats:
                stats[example] = 1
            else:
                stats[example] +=1
        maxk = None
        maxv = 0
        for key in stats:
            if stats[key] > maxv:
                maxv = stats[key]
                maxk = key
        return maxk
    else:
        return 'Female'

##############################################################
#####THE ALGORITHM###########################################
##############################################################

#examples is a list of lists, each sublist is a data point
#attributes are the 'columns' of our data
#default is whatever we want to be
def DTL(examples, attributes, default, targetstring, recur_until):
    target = attributes.index(targetstring)
    
    if len(examples) <= 1:
        return default
        #return tree

    #a pruning step
    elif len(attributes) <= recur_until:
        return ComputeMode(examples, target) 
        #return tree

    similar = examples[0][target]
    differences = 1
    total = 0
    for e in examples:
        if e[target] != similar:
            differences += 1
        total += 1

    #here we prune again, this improves performance a bit
    if differences/total <= 0.1 or differences == 1:
        return similar
        #return tree
    
    else:
        best = ChooseAttribute(attributes, examples, targetstring)
        #print(attributes[best])
        tree = Tree(children = {}, data = attributes[best])
        #for each value the attribute 'best' can take on
        for value in data_dict[attributes[best]]:
            expls = [copy.copy(i) for i in examples if i[best] == value]
            mode = ComputeMode(expls, target)
            for row in expls:
                del row[best]
            #copying attributes and removing best
            copy_attributes = list(copy.copy(attributes))
            copy_attributes.remove(attributes[best])
            subtree = DTL(expls, copy_attributes, mode, targetstring, recur_until)
            tree.children[value] = subtree
        #checking for issues
        for child in tree.children:
            if isinstance(child, str):
                continue
            if tree.children[child].data  == tree.data:
                print('TREE ERROR')
        return tree

#############################################################
###############MAIN##########################################
#############################################################


#getting rid of bad data columns, they overfit the data horribly
nrwlabels = data[0][0:8] + data[0][10:]
nrwdata = [x[0:8] + x[10:] for x in data[1:]]

targetval = 4
targetname = nrwlabels[targetval]
#normalizing male and female occourances
counter = 0
stop = sum([1 if i[targetval] == 'Female' else 0 for i in nrwdata])
newdata = []
males = 0
females = 0

print('normalizing')
for entry in nrwdata:
    if entry[targetval] == 'Female':
        newdata.append(entry)
        females +=1
    elif entry[targetval] == 'Male' and counter < stop:
        newdata.append(entry)
        males += 1
        counter +=1
print('done normalizing')
print('male/female ratio is: ' + str(males/females))

for i in range(10, 0, -1):
    training_data = nrwdata[0:195856]
    test_data1 = nrwdata[:195856]
    oneRTree = DTL(training_data, nrwlabels, None, targetname, i)
    
    #graphing
    printer = PrintTree()
    printer.printTree(oneRTree.toDict())
    #getting error
    oneRerrors = 0
    for dp in test_data1:
        classification = oneRTree.classifySingle(dp, nrwlabels)
        if classification != dp[targetval]:
            oneRerrors += 1
        
    print('for compute depth ' + str(i) + ' error rate is: ' + str(oneRerrors/len(test_data1)))






















        
