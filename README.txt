Tree printing subroutine was modified from https://llimllib.github.io/pymag-trees/.

Id3 takes one input, the dataset.

To run id3.py, in the terminal go to the src directory and then enter the following:
python id3.py "../data/A3ProcessedData.csv"

Id3's output will print out the results and error for each tree.